<?php

/**
 * Form builder; Drush filelog form.
 *
 * @see system_settings_form()
 * @ingroup forms
 */
function drush_filelog_settings() {
  $form = array();
  $form['description'] = array(
    // TO-DO: Description?
    '#markup' => '<p>' . t('Here be dragons') . '</p>',
  );
  // Log file path
  $form['drush_filelog_file_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Log file path'),
    '#description' => t('Absolute file path for log file.'),
    '#default_value' => variable_get('drush_filelog_file_path', _drush_filelog_default_file_path()),
  );
  // Log file permission
  $form['drush_filelog_file_permission'] = array(
    '#type' => 'textfield',
    '#title' => t('Log file permissions'),
    '#description' => t('Usually drush is run by different user(s) than web server. Every user that can run drush should also have permissions to write into log file. Default: <b>@permission</b>', array('@permission' => DRUSH_FILELOG_DEFAULT_FILE_PERMISSION)),
    '#default_value' => variable_get('drush_filelog_file_permission', DRUSH_FILELOG_DEFAULT_FILE_PERMISSION),
  );
  // Log file format
  $form['drush_filelog_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Log file format'),
    '#description' => t("Have to see when I know what are the possibilities"),
    '#default_value' => variable_get('drush_filelog_format', _drush_filelog_default_format()),
  );
  // Exclude message types
  $form['drush_filelog_exclude_message_types'] = array(
    '#type' => 'textfield',
    '#title' => t('Exclude message types'),
    '#description' => t("Separate message types that you wan't to exclude from logging with comma. Common types are 'warning', 'error', 'success' and 'notice'. Ie. <i>notice, success</i>"),
    '#default_value' => implode(', ', variable_get('drush_filelog_exclude_message_types', array())),
  );
  // Attach submit handler
  $form['#submit'][] = 'drush_filelog_settings_submit';
  return system_settings_form($form);
}

/**
 * Form submit handler for drush_filelog_settings().
 */
function drush_filelog_settings_submit($form, &$form_state) {
  // Change exclude message types from string to array
  $submitted_types = explode(',', $form_state['values']['drush_filelog_exclude_message_types']);
  $exclude_message_types = array();
  foreach ($submitted_types as $message_type) {
    $message_type = trim($message_type);
    if (!empty($message_type)) {
      $exclude_message_types[] = $message_type;
    }
  }
  // Replace submitted value with array to be saved
  $form_state['values']['drush_filelog_exclude_message_types'] = $exclude_message_types;
}
