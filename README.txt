Missing features / Misc:
 - Set limit for log size? -> remove lines in cron run
 - Do we want to have option to disable normal drush output, sounds kinda stupid.. :D
 - Generic check if the log file is writable
 - Browse log from admin
 - Hooks for implementing other filters
 - Hooks for implementing own format args
 - Creating better way of defining log format and handling args
 - Add username who is running the drush?