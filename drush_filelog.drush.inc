<?php

/**
 * Implements hook_drush_init().
 */
function drush_filelog_drush_init() {
  // Change logging callback to our own
  drush_set_context('DRUSH_LOG_CALLBACK', '_drush_filelog_write_log');
}

/**
 * Custom DRUSH_LOG_CALLBACK function for log file writing
 */
function _drush_filelog_write_log($entry) {
  static $file_path, $exclude_message_types, $format;
  // Check only once for writable log file
  if (is_null($file_path)) {
    $file_path = _drush_filelog_writable_file_path();
  }
  if (is_null($exclude_message_types)) {
    $exclude_message_types = variable_get('drush_filelog_exclude_message_types', array());
  }
  if (is_null($format)) {
    $format = variable_get('drush_filelog_format', _drush_filelog_default_format());
  }
  if ($file_path && !in_array($entry['type'], $exclude_message_types)) {
    $entry_args = _drush_filelog_format_entry_args($entry);
    // TO-DO: Should we use format_string or similar instead of str_replace?
    file_put_contents($file_path, str_replace(array_keys($entry_args), array_values($entry_args), $format) . "\n", FILE_APPEND);
  }
  // Call original log function
  _drush_print_log($entry);
}
